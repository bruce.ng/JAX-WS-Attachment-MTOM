	A complete JAX-WS SOAP-based example to show how to use Message Transmission Optimization Mechanism (MTOM)
and XML-Binary Optimized Packaging (XOP) technique to send a binary attachment (image) from server to client
and vice verse

Reference
	http://www.devx.com/xml/Article/34797/1763/page/1
	http://download.oracle.com/docs/cd/E17802_01/webservices/webservices/docs/2.0/jaxws/mtom-swaref.html
	http://www.crosschecknet.com/intro_to_mtom.php
	http://download.oracle.com/docs/cd/E12840_01/wls/docs103/webserv_adv/mtom.html
	http://www.theserverside.com/news/1363957/Sending-Attachments-with-SOAP
	http://metro.java.net/guide/Binary_Attachments__MTOM_.html
	
Enabling MTOM on server
	Enable server to send attachment via MTOM is very easy, just annotate the web service implementation class
with javax.xml.ws.soap.MTOM.

ảnh truyền trong webservice đươc mã hóa dạng: base64Binary


P.S The first wsdl request is omitted to save space.
